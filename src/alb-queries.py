#!/usr/bin/env python3

import requests
import jsons
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.info("Running VPC Debug script")

def lambda_function(event, context):
    logger.info("Lambda invoked")
    control = requests.Session()
    control.headers.update({'host': 'exizent.dev'})
    response = control.get('https://3.9.166.67', allow_redirects=False, verify=False)
    logger.info(jsons.dump(response.headers))


    s =  requests.Session()
    control.headers.update({'host': 'exizent.com'})
    respone = s.get('https://10.1.8.90:8443/ipsendpoint')
    logger.info(jsons.dump(response.headers))

    # # Against private ALB
    s =  requests.Session()
    control.headers.update({'host': 'exizent.com'})
    response = s.get('https://10.1.18.21:8443/ipsendpoint')
    logger.info(jsons.dump(response.headers))

if __name__ == '__main__':
    print("python VPC debug function")
    lambda_function("dummy1", "dummy2")