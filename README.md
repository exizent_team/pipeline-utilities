# Bitbucket Pipeline Utilities #

This repository provides a set of scripts to support bitbucket pipelines
___

## cloudformation_deploy_validation.py
### Summary
A python script that returns exit code 2 if the stack status does not return "UPDATE_COMPLETE" or "CREATE_COMPLETE. 
This will be used to validate the infrastructure has been deployed successfully and will be run from pipelines such as `exizent-frontend` and `exizent-backend`.

usage: `python src/cloudformation_deploy_validation.py <stack_name>`

___

## get_cluster_name.sh
### Summary
A bash script that exports a cluster name in an environment variable, since cluster names are dynamically generated and have random strings.

usage: `source src/get_cluster_name.sh exizent-backend-sandbox-Cluster`

___

## ecr-tag-updater.sh
### Summary
A bash script that checks if an ECR image has been tagged with an environment (ie, sandbox, develop, testing, production), and if it hasn't been tagged, tags it.

usage: `source src/ecr-tag-updater.sh <environment>`

___

## validate-bucket.sh
### Summary
A bash script to help with dynamically named S3 buckets (ie, file-storage-service S3 ALB logs). It takes the expected name of the S3 bucket (the bucket name without the randomly created string appended) and a tag to validate against, to ensure the correct bucket name is being returned.

usage: `source src/validate-bucket.sh <environment> <bucket name> <ResourceTag value or none>`

___

## cloudformation_delete_validation.py
### Summary
A python script to poll the cloudformation stack for an amount of time. It returns exit code 0 if it's successful, or exit code 2 if the stack is not deleted within that time.

usage: `python src/cloudformation_delete_validation.py <stack_name> <max_attempts> <delay>`

___

## delete_s3_bucket_objects.py
### Summary
A python script to delete all the objects (and previous versions of that object) in an S3 bucket. 
This can be used to prevents "bucket not empty" errors when trying to delete an S3 bucket with versioning enabled, e.g. when tearing down development stacks.

usage: `python src/delete_s3_bucket_objects.py <bucket_name>`

___

## aws-authorisation-script.sh
### Summary
A bash script that authorises the pipeline step against an AWS account (ie, sandbox or production) using OpenID Connect.

usage: `source src/aws-authorisation-script.sh <environment>`

___

## update_secrets.py
### Summary
Updates the secret values stored in AWS Secrets Manager based on BitBucket repository variables (access is restricted to BitBucket admins). It's currently to be used in exizent-backend-infx when a stack is deployed, to ensure secrets persist between deployments. 

usage: `python3 src/update_secrets.py <string>`

The string argument is set in the pipeline step using a text file which contains the environment variable names. This file is likely to become redundant in the future as a refactor how we manage our secrets.

___

## check_build_number.py
### Summary
Checks the build number of an environment, to be used in a pipeline deployment
step to prevent automated tests from running if the build number is incorrect

usage: `python3 src/check_build_number.py <service> <cluster> <task_definition> <optional: env/vars endpoint> <optional: build number>`

___

## msteams_bb_deployment_status.py
### Summary
Utility to allow consistent deployment status messages to be provided to bitbucket

```
usage: msteams_bb_deployment_status.py [-h] --stage {STARTING,IN_PROGRESS,COMPLETED,FAILED} [--build BUILD] [--pipeline_url PIPELINE_URL] web_hook service env

Send deployment to Teams Channels

positional arguments:
  web_hook              Provide the webhook URL for a Teams channel
  service               The service being deployed
  env                   The environment being deployed to, e.g. prod, test

optional arguments:
  -h, --help            show this help message and exit
  --stage {STARTING,IN_PROGRESS,COMPLETED,FAILED}
                        The pipeline stage/status
  --build BUILD         The build or release number being deployed
  --pipeline_url PIPELINE_URL
                        The url for the pipeline

``` 
### running locally

Set up the virtual env
In order to run tests you must set up a local environment variable called TEAMS_TEST_WEBHOOK and set it's value to a teams webhook connector URL
```
virtualenv env
.\env\Scripts\activate
pip install -r requirements-dev.txt
pip install -r requirements.txt

nosetests -sv src/test_*
```

### BitBucket Pipelines Config

* Add a workspace variable called  TEAMS_DEPLOY_CHANNEL_URL - suggest it is secret to prevent the webhook URL being shared
* Add the following commands to your pipeline to clone and run ther script, adjust parameters as required.

```
git clone git@bitbucket.org:exizent_team/teams-connectors.git

pip install -r requirements.txt

./src/msteams_bb_deployment_status.py --stage STARTING --build $BITBUCKET_BUILD_NUMBER --pipeline_url "https://bitbucket.org/$BITBUCKET_REPO_FULL_NAME/addon/pipelines/home#!/results/$BITBUCKET_PIPELINE_UUID" $TEAMS_DEPLOY_CHANNEL_URL $BITBUCKET_REPO_FULL_NAME $BITBUCKET_DEPLOYMENT_ENVIRONMENT
```
___
# Exizent pipeline tooling image

Scripts included in this image are set in `Dockerfile`. The image is built from the `develop` step in the pipeline for this repository.