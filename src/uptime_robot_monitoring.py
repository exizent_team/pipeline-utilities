#!/usr/bin/env python3
''' Script for use inside bitbucket pipelines to ensure
    any public APIs we monitor have a good Uptime Robot
    external monitor configured.
    API key is set organisation-wide as bitbucket variable'''
import os
import sys
import json
import getopt
import requests

api_key = os.environ['UPTIME_ROBOT_API_KEY']
if os.environ.get('ENVIRONMENT') is not None:
    ENVIRONMENT = os.environ['ENVIRONMENT']
elif os.environ.get('Environment') is not None:
    ENVIRONMENT = os.environ['Environment']
else:
    ENVIRONMENT = ''

''' Contact IDs can be pulled from the API using a read-only API key
    https://uptimerobot.com/#getAlertContactsWrap '''
alert_contact_ids = '4040633_0_0-3009849_0_0-3315647_0_0'

def check_monitor_exists(host_name):
    ''' Query UptimeRobot API to find if an uptime robot monitor
    exists for a given DNS hostname.
    The API returns a 200 for unauthenticated responses so we have
    to handle API errors described inside json response rather than
    rely on sane HTTP status codes from the API. '''

    mon_id = False
    try:
        response = requests.post(
            'https://api.uptimerobot.com/v2/getMonitors',
            data={'api_key': api_key},
            timeout=5)
    except requests.exceptions.Timeout:
        print('Timeout calling UptimeRobot API - maybe try again soon?')
        sys.exit(2)
    except requests.exceptions.TooManyRedirects:
        print('Too Many Redirects error when calling UptimeRobot API')
        sys.exit(2)
    except requests.exceptions.ConnectionError:
        print('Unexpected connectionError calling UptimeRobot API')
        sys.exit(2)
    except requests.exceptions.RequestException:
        print('Catastrophic calling UptimeRobot API')
        sys.exit(2)

    current_monitors = json.loads(response.text)
    if current_monitors['stat'] != 'ok':
        print('API request status ' + current_monitors['stat'])
        print('API responded with a 200 but failed for some other reason')
        print(json.dumps(current_monitors['error']))
        sys.exit(2)
    for monitor in current_monitors['monitors']:
        if monitor['friendly_name'] == host_name:
            mon_id = monitor['id']
    return mon_id


def add_uptime_monitor(host_name, path):
    ''' Add a new Uptime monitor enpoint in a standard fashion
        If monitor exists, return the ID of the monitor'''
    try:
        response = requests.post(
            'https://api.uptimerobot.com/v2/newMonitor',
            data={
                'api_key': api_key,
                'friendly_name': host_name,
                'url': 'https://' + host_name + '/' + path,
                'type': 2,
                'keyword_type': 2,
                'keyword_case_type': 0,
                'keyword_value': 'Healthy',
                'interval': 300,
                'alert_contacts': alert_contact_ids,
                'ignore_ssl_errors': 1,
                'disable_domain_expire_notifications': 1
            },
            timeout=5)
    except requests.exceptions.Timeout:
        print('Timeout calling UptimeRobot API - maybe try again soon?')
        sys.exit(2)
    except requests.exceptions.TooManyRedirects:
        print('Too Many Redirects error when calling UptimeRobot API')
        sys.exit(2)
    except requests.exceptions.ConnectionError:
        print('Unexpected connectionError calling UptimeRobot API')
        sys.exit(2)
    except requests.exceptions.RequestException:
        print('Catastrophic calling UptimeRobot API')
        sys.exit(2)
    new_monitor = json.loads(response.text)
    if new_monitor['stat'] != 'ok':
        print('API request status ' + new_monitor['stat'])
        print('API responded with a 200 but failed for some other reason')
        print(json.dumps(new_monitor['error']))
        sys.exit(2)
    else:
        print('Monitoring endpoint created successfully')

def edit_uptime_monitor(host_name, monitor_id, path):
    ''' Edit existing UR monitor to make sure it's consistent
        Contact details discovered via getAlertContacts API endpoint'''
    try:
        response = requests.post(
            'https://api.uptimerobot.com/v2/editMonitor',
            data={
                'id': monitor_id,
                'api_key': api_key,
                'friendly_name': host_name,
                'url': 'https://' + host_name + '/' + path,
                'type': 2,
                'keyword_type': 2,
                'keyword_case_type': 0,
                'keyword_value': 'Healthy',
                'interval': 300,
                'alert_contacts': alert_contact_ids,
                'ignore_ssl_errors': 1,
                'disable_domain_expire_notifications': 1
            })
    except requests.exceptions.Timeout:
        print('Timeout calling UptimeRobot API - maybe try again soon?')
        sys.exit(2)
    except requests.exceptions.TooManyRedirects:
        print('Too Many Redirects error when calling UptimeRobot API')
        sys.exit(2)
    except requests.exceptions.ConnectionError:
        print('Unexpected connectionError calling UptimeRobot API')
        sys.exit(2)
    except requests.exceptions.RequestException:
        print('Catastrophic calling UptimeRobot API')
        sys.exit(2)
    new_monitor = json.loads(response.text)
    if new_monitor['stat'] != 'ok':
        print('API request status ' + new_monitor['stat'])
        print('API responded with a 200 but failed for some other reason')
        print(json.dumps(new_monitor['error']))
        sys.exit(2)
    else:
        print("Uptime Robot Monitor configuration is consistent")


def main(argv):
    ''' If monitor matching DNS name does not exizt, create it
        If it does already exists, just make sure it's set consistently '''

    if ENVIRONMENT == 'sandbox':
        print('Sandbox environment: no monitoring being configured')
        sys.exit(0)

    host = ''
    path = ''
    try:
        opts, args = getopt.getopt(argv, "h:u:", ["host=", "healthcheck_uri="])
    except getopt.GetoptError:
        print(
            'usage: uptime-robot-monitoring.py '
            '-h <dns_name> -u <healthcheck_uri>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--help':
            print(
                'usage: uptime-robot-monitoring.py '
                '-h <dns_name> -u <healthcheck_uri>')
            sys.exit()
        elif opt in ("-h", "--host"):
            host = arg
        elif opt in ("-u", "--uri"):
            path = arg

    print("Checking monitor exists for " + host)
    monitor_id = check_monitor_exists(host)
    if monitor_id is not False:
        print("Monitor already in place")
        edit_uptime_monitor(host, monitor_id, path)
        sys.exit(0)

    else:
        print("No monitor matching URL is in place, attempting to add")
        add_uptime_monitor(host, path)


if __name__ == "__main__":
    main(sys.argv[1:])
