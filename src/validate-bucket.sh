#!/bin/bash

set -e

if [[ $# != 3 ]]
    then
        echo "Error: environment, bucket name and ResourceTag value to validate required, e.g. \"sandbox exizent-file-storage-sandbox-s3bucketlogs sandbox-file-storage-service \""
        echo "If there is no ResourceTag for the resource, add \"none\""
        exit 2
fi

echo "Looking for bucket with name: ${2}"
echo "Checking in ${1} environment"

unset BUCKET_NAME

export BUCKET_INFO=$(aws s3 ls | grep ${1} | grep ${2})
echo $BUCKET_INFO

IFS=" "
read -ra ADDR <<< $BUCKET_INFO
export BUCKET_NAME=${ADDR[2]}

export TAGS=$(aws s3api get-bucket-tagging --bucket $BUCKET_NAME | jq '.TagSet[] | select(.Key=="ResourceTag")' | jq .Value | tr -d '"')

echo "${3} ---- $TAGS"

if [[ ${3} != *$TAGS* ]]; then
    echo "The tags on this bucket do not match the argument given: ${3}"
    exit 2
fi

echo "Bucket validated against ResourceTag: ${3}"
echo "Bucket name: $BUCKET_NAME"