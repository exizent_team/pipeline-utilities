#!/usr/bin/env python3
import pymsteams
import validators
import sys
import argparse
import enum

class StageEnum(enum.IntEnum):
    STARTING = 1
    IN_PROGRESS = 2
    COMPLETED = 3
    FAILED = 4
    TORN_DOWN = 5

    # magic methods for argparse compatibility

    def __str__(self):
        return self.name.upper()

    def __repr__(self):
        return str(self)

    @staticmethod
    def argparse(s):
        try:
            return StageEnum[s.upper()]
        except KeyError:
            return s

def get_card_color(stage):
    print(stage)
    if stage == StageEnum.STARTING:
        return "#FFC599"
    elif stage == StageEnum.IN_PROGRESS:
        return "#FF8B33"
    elif stage == StageEnum.COMPLETED: 
        return "#00D7FF"
    elif stage == StageEnum.FAILED: 
        return "#FF0000"
    elif stage == StageEnum.TORN_DOWN: 
        return "#00D7FF"
    else:
        return "#FFFFFF"

def get_card_image(stage):
    if stage == StageEnum.STARTING:
        return "img/starting.jpg"
    elif stage == StageEnum.IN_PROGRESS:
        return "img/in_progress.jpg"
    elif stage == StageEnum.COMPLETED: 
        return "img/completed.jpg"
    elif stage == StageEnum.FAILED: 
        return "img/failed.jpg"
    elif stage == StageEnum.TORN_DOWN: 
        return "img/completed.jpg"
    else:
        return "img/default.jpg"

def send_msg(webhook, env, build, pipeline_url, stage, service):

    try:
        teams_msg = pymsteams.connectorcard(webhook)
        teams_msg.text(service + ' build version "' + build + '" to ' + env.upper() + ' is ' + stage.name)

        ## Add link to JIRA deployments dashboard
        teams_msg.addLinkButton("View Deployments","https://exizent.atlassian.net/jira/software/c/projects/ES/deployments")
        ## Add a link button to the deployment pipeline if a URl is available
        if (pipeline_url):
            teams_msg.addLinkButton("View Pipeline", pipeline_url)

        ## Set the card color based upon the stage
        teams_msg.color(get_card_color(stage))
        # send the message.
        teams_msg.send()

        return teams_msg.last_http_response.status_code
    except Exception as ex:
        print("******  ERROR Sending message to Teams ******")
        print(ex)
        return 500


def main():
    parser = argparse.ArgumentParser(description='Send deployment to Teams Channels')
    parser.add_argument('web_hook', help='Provide the webhook URL for a Teams channel')
    parser.add_argument('service', help='The service being deployed')
    parser.add_argument('env', help='The environment being deployed to, e.g. prod, test')
    parser.add_argument('--stage',help='The pipeline stage/status',type=StageEnum.argparse, choices=list(StageEnum), required=True)
    parser.add_argument('--build', help='The build or release number being deployed', default="Unknown")
    parser.add_argument('--pipeline_url', help='The url for the pipeline', default="https://bitbucket.org/dashboard/overview")
    
    args = parser.parse_args()
    if (not validators.url(args.web_hook)):
        parser.error("ERROR: web_hook must be a valid URL")
    if (args.pipeline_url and not validators.url(args.pipeline_url)):
        print("URL given was: " + str(args.pipeline_url) )
        parser.error("ERROR: pipeline url must be a valid URL: " + str(args.pipeline_url) )

    else:
        status = send_msg(args.web_hook, args.env, args.build, args.pipeline_url, args.stage, args.service)
        print('Response: ' + str(status))

if __name__ == '__main__':
    main()