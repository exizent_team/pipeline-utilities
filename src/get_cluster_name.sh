#!/bin/bash

set -e

if [[ $# != 1 ]]
    then
        echo "Error: Cluster argument required, e.g. \"exizent-backend-sandbox-Cluster \""
        exit 2
fi
echo "Using AWS KEY $AWS_ACCESS_KEY_ID"
IDENTITY=$(aws sts get-caller-identity)
echo $IDENTITY | jq
export CLUSTER_NAME=$(aws ecs list-clusters | jq .clusterArns | grep ${1} | awk -F'"' '{print $2}' | awk -F'/' '{print $2}')
echo $CLUSTER_NAME