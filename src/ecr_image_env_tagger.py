#!/usr/bin/env python3
''' Script for use inside bitbucket pipelines to safely
tag an build of an image for an environment and handle the
event that the image has already been tagged, e.g. when we
want to re-run a pipeline, where the tagging step in a simple
cli bash script would fail with an error '''

import sys
import getopt
import boto3


def get_ecr_image_manifest(repository, build_id):
    ''' Retrieve the ECR image manifest for the image associated
        with the given a bitbucket build number '''
    client = boto3.client("ecr")
    try:
        response = client.batch_get_image(
            repositoryName=repository,
            imageIds=[
                {
                    'imageTag': build_id
                },
            ],
            acceptedMediaTypes=[
                'string',
            ]
        )
        return response['images'][0]['imageManifest']
    except client.exceptions.ImageNotFoundException:
        print(
            f'No ECR image found matching build: {build_id} '
            f' in repo: {repository}'
            'This would indicate previous build or ECR push has failed '
            'please investigate')
        sys.exit(2)
    except client.exceptions.InvalidParameterException:
        print('Invalid parameters given - check your pipeline setup')
        sys.exit(2)
    except client.exceptions.RepositoryNotFoundException:
        print('ECR repo not found - check your pipeline setup')
        sys.exit(2)
    except client.exceptions.ServerException:
        print('AWS returned a ServerException error to the query')
        return False


def get_ecr_image_info(repository, build_id):
    ''' Describe ECR image for given repo tagged to a build_id
        in order to find out if the image has been tagged to a
        particular dev environment '''
    client = boto3.client("ecr")
    try:
        response = client.describe_images(
            repositoryName=repository,
            imageIds=[
                {
                    'imageTag': build_id
                },
            ],
        )
    except client.exceptions.ImageNotFoundException:
        print(
            f'No ECR image found matching build: {build_id} '
            f'in repo: {repository} '
            'This would indicate previous build or ECR push has failed '
            '- please investigate')
        sys.exit(2)
    except client.exceptions.InvalidParameterException:
        print('Invalid parameters given - check your pipeline setup')
        sys.exit(2)
    except client.exceptions.RepositoryNotFoundException:
        print('ECR repo not found - check your pipeline setup')
        sys.exit(2)
    except client.exceptions.ServerException:
        print('AWS returned a ServerException error to the query')
        sys.exit(2)
    return response


def tag_image_to_environment(repository, environment, manifest):
    ''' Tag the image associated with a build_id to the given
        development  environment '''
    client = boto3.client("ecr")
    try:
        response = client.put_image(
            repositoryName=repository,
            imageManifest=manifest,
            imageTag=environment
        )
        status_code = response['ResponseMetadata']['HTTPStatusCode']
        print(f'AWS Response: {status_code}')
        if status_code == 200:
            print(f'Image tagged with tag {environment} successfully')
        else:
            print('Re-tag request to AWS API failed unexpectedly')
    except client.exceptions.ImageNotFoundException:
        print('No ECR image found matching manifest')
        return False
    except client.exceptions.ImageTagAlreadyExistsException:
        print(f'ECR Image for given build already tagged to {environment}')
        print('This should not have happened - is another deployment running?')
        return False
    except client.exceptions.RepositoryNotFoundException:
        print('ECR repo not found - check your pipeline setup')
        sys.exit(2)
    except client.exceptions.ServerException:
        print('AWS returned a ServerException error to the query')
        sys.exit(2)
    return response


def check_if_already_tagged(image_details, environment):
    ''' Return true only if an image tagged to a build_id is also
        tagged to the given development environment '''
    if environment in image_details['imageDetails'][0]['imageTags']:
        print('Image currently tagged for environment: ' + environment)
        return True
    print('Image not currently tagged for environment: ' + environment)
    return False


def main(argv):
    ''' Main entry point for ECS image (re-)tagger.
    Retrieves all current image information, checks if image is already tagged
    and exit cleanly if it is .If it isn't tag for environment and exit to let
    the pipeline continue. '''
    repo = ''
    build_id = ''
    environment = ''
    try:
        opts, args = getopt.getopt(
            argv, "r:b:e:",
            ["repo=", "build=", "environment="])
    except getopt.GetoptError:
        print(
            'usage: tag_and_redeploy_ecs.py -r '
            '<ecr_repo_name> -u <bitbucket_build_id>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--help':
            print(
                'usage: tag_and_redeploy_ecs.py -r '
                '<ecr_repo_name> -u <bitbucket_build_id>')
            sys.exit()
        elif opt in ("-r", "--repo"):
            repo = arg
        elif opt in ("-b", "--build"):
            build_id = arg
        elif opt in ("-e", "--environment"):
            environment = arg
    print(f'{repo} - {build_id} - {environment}')
    image_info = get_ecr_image_info(repo, build_id)
    if check_if_already_tagged(image_info, environment) is False:
        manifest = get_ecr_image_manifest(repo, build_id)
        tag_image_to_environment(repo, environment, manifest)


if __name__ == "__main__":
    main(sys.argv[1:])
