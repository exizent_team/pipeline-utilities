#!/usr/bin/env python3
import requests
import time
import sys
import boto3
import botocore

"""
Compare the build number on the service with the build number from the pipeline
"""
def check_build_number(url, build):
    request = requests.get(url)
    request_text = request.text
    if request_text.strip() == build:
        print("Build number is correct - exiting with exit code 0")
        sys.exit(0)
    if request_text.strip() == "":
        print("There is no build number. Please check the static file exists for the application.")
        sys.exit(2)
    else:
        print("Build number is not correct")
        print("Waiting 10 seconds")
    print("Exiting with exit code 2")
    sys.exit(2)

"""
Poll the ECS API every 15 seconds until a successful state is reached. An error is returned after 40 failed checks.
"""
def check_stable_build(service, cluster, task_definition, ecs):
    waiter = ecs.get_waiter("services_stable")
    print("Waiter {}".format(waiter))
    try:
        print("waiting for {} to become stable".format(service))
        waiter.wait(cluster=cluster, services=[service])
        print("{} is stable".format(service))
    except botocore.exceptions as ex:
        logger.error("The service didn't become stable. {}".format(ex))

def main():
    service = sys.argv[1]
    cluster = sys.argv[2]
    task_definition = sys.argv[3]
    url = None
    build = None
    print(len(sys.argv))
    if len(sys.argv) > 4:
        url = sys.argv[4]
        build = "BITBUCKET_BUILD_NUMBER={}".format(sys.argv[5])
    session = boto3.session.Session()
    ecs = session.client("ecs")
    ecs.update_service(cluster=cluster, service=service,
            taskDefinition=task_definition )
    check_stable_build(service, cluster, task_definition, ecs)
    if None not in (url, build):
        print("Waiting 15 seconds to test the build number")
        time.sleep(15)
        check_build_number(url, build)

if __name__ == "__main__":
    main()
