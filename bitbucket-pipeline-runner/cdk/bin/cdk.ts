#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { BitbucketPipelineRunnerStack } from '../lib/cdk-stack';
const awsEnvironment = process.env.DEPLOYMENT_ENV || 'sandbox'
const prodAccountId = process.env.CDK_DEFAULT_ACCOUNT || '240676735054'
const region = process.env.CDK_DEFAULT_REGION || 'eu-west-2'
const sandboxAccountId = process.env.CDK_DEFAULT_ACCOUNT || '143704233432'

if (awsEnvironment === 'testing' || awsEnvironment === 'production') {
  var AwsAccountId = prodAccountId
} else {
  var AwsAccountId = sandboxAccountId
};

const app = new cdk.App();

new BitbucketPipelineRunnerStack(app, `BitbucketPipelineRunnerStack-${awsEnvironment}`, {
  awsEnvironment,
  env: {
    account: AwsAccountId,
    region
  }
});