#!/usr/bin/env python3
import boto3
import sys
import botocore

def check_stack_is_deployed(stack_name):
    cloudformation = boto3.resource('cloudformation')
    stack = cloudformation.Stack(stack_name)
    status = stack.stack_status
    if status == "UPDATE_COMPLETE" or status == "CREATE_COMPLETE":
        return True
    print(f"The status of stack {stack_name} is {status} - please make sure the cloudformation for {stack_name} has been deployed properly via the correct pipeline.")
    return False

def main():
    stack_name = sys.argv[1]
    status = check_stack_is_deployed(stack_name)
    if status is True:
        sys.exit(0)
    print("")
    sys.exit(2)

if __name__ == "__main__":
    main()