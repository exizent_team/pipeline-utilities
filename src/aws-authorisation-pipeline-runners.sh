#!/bin/bash
#
# Authenticate a Bitbucket pipeline session using chained IAM roles Exizent AWS env
# Requires the OIDC role to also allow the Bitbucket pipeline EC2 role to assume it
# Production or Sandbox
# Script assumes the following Reposity Variables are set for your project
# - $AWS_DEFAULT_REGIONS
# - $AWS_OIDC_ROLE_SANDBOX
# - $AWS_OIDC_ROLE_PRODUCTION

set -e

if [[ $# != 1 ]]
    then
        echo "Error: Environment argument required, e.g. \"aws-auth sandbox\""
        exit 2
fi

if [[ -z "${AWS_DEFAULT_REGION}" ]]; then
    echo "Error: no Bitbucket Repository variable for AWS_DEFAULT_REGION is set in Bitbucket repository settings"
    exit 2
fi

AWS_AUTH_ENVIRONMENT=${1}

if [[ "$AWS_AUTH_ENVIRONMENT" == "develop" ]]; then
    AWS_AUTH_ENVIRONMENT="sandbox"
fi
if [[ "$AWS_AUTH_ENVIRONMENT" == "testing" ]]; then
    AWS_AUTH_ENVIRONMENT="production"
fi
if [[ "$AWS_AUTH_ENVIRONMENT" == "logging" ]]; then
    AWS_AUTH_ENVIRONMENT="logging"
fi

OIDC_ROLE=AWS_OIDC_ROLE_${AWS_AUTH_ENVIRONMENT^^}
export AWS_REGION=$AWS_DEFAULT_REGION
export AWS_ROLE_ARN=${!OIDC_ROLE}

if [[ -z "${!OIDC_ROLE}" ]]; then
    echo "Error: no Bitbucket Repository variable for IAM Role ${OIDC_ROLE} is set"
    exit 2
fi

echo "Role ARN: ${AWS_ROLE_ARN}"

sts_creds=$(aws sts assume-role --role-arn $AWS_ROLE_ARN --role-session-name pipeline-session --duration-seconds 1000 --output json)
export AWS_ACCESS_KEY_ID=$(echo ${sts_creds} | jq -r '.Credentials.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo ${sts_creds} | jq -r '.Credentials.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo ${sts_creds} | jq -r '.Credentials.SessionToken')

if [[ -z "${AWS_ACCESS_KEY_ID}" || -z "${AWS_SECRET_ACCESS_KEY}" || -z "${AWS_SESSION_TOKEN}" ]]; then
    echo "Unknown error: AWS credential variables haven't been set"
    exit 2
else
    echo "AWS Credentials successfully set"
    echo "Using AWS KEY $AWS_ACCESS_KEY_ID"
fi
