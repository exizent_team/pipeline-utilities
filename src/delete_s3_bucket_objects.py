#!/usr/bin/env python3
import sys
import boto3

def main():
    bucket_name = sys.argv[1]
    session = boto3.Session()
    s3 = session.resource(service_name='s3')
    bucket = s3.Bucket(bucket_name)
    bucket.object_versions.delete()

if __name__ == "__main__":
    main()