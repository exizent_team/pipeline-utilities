#!/usr/bin/env python3
"""Script to test that key DNS entries are present and resolving against
specific hostedZones when querying Ruote53 nameservers.
Intended to detect bad changes prior to deployment to Production
** Will only detect if entries have been removed or broken **
Records pointing to wrong address will not be detected"""

import sys
import logging
import dns.resolver
import boto3
import botocore
import json

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

def get_delegationset_nameservers(hostedzone_id):
    """Return a list of Nameservers for a given HostedZone ID"""
    dns.resolver.default_resolver = dns.resolver.Resolver(configure=False)
    dns.resolver.default_resolver.nameservers = [ '8.8.8.8', '8.8.1.1' ]
    client = boto3.client('route53')
    try:
        logger.info('Calling Route53 API on desired HostedZone (%s)', hostedzone_id)
        response = client.get_hosted_zone(
            Id=hostedzone_id
        )
    except botocore.exceptions.NoCredentialsError:
        logger.error('NoCredentialsError: There is an issue with IAM secrets or role permissions')
        sys.exit(1)
    except botocore.exceptions.ClientError:
        logger.error('Unknown API Error occurred')
        sys.exit(1)
    logger.info('DNS server returned is %s', response['DelegationSet']['NameServers'][0])
    try:
        nameserver_ips = dns.resolver.resolve(response['DelegationSet']['NameServers'][0], 'A')
    except dns.resolver.NoNameservers:
        logger.error("No name servers available - using a working recursive nameserver?")
        sys.exit(1)
    return nameserver_ips

def check_resolution_route53(dnshost, hostedzone_id, record_type):
    """Test name given can resolve against the Route53 endpoint for the HostedZone"""
    nameservers = get_delegationset_nameservers(hostedzone_id)
    dns.resolver.default_resolver = dns.resolver.Resolver(configure=False)
    dns.resolver.default_resolver.nameservers = [ str(nameservers[0]) ]
    try:
        logging.info('Testing DNS %s record for %s against HostedZone ID %s using resolver %s', \
            record_type, dnshost, hostedzone_id,str(nameservers[0]))
        answers = dns.resolver.resolve(dnshost, record_type)
        if record_type == 'A':
            for resource_record in answers:
                logger.info(resource_record.address)
        logging.info('DNS record for %s against HostedZone ID %s successfully resolved', \
            dnshost, hostedzone_id)
    except dns.resolver.NoAnswer:
        logger.error("Error: No answer received from Route53 nameservers -\
            may be an intermittent DNS failure or maybe the HostedZone information has changed")
        sys.exit(1)
    except dns.resolver.NXDOMAIN:
        logger.error("Error: NXDOMAIN received - this is very, very wrong, investigate urgently")
        sys.exit(1)
    except dns.resolver.Timeout:
        logger.error("Error: Timeout from querying Route53 DNS endpoint")
        sys.exit(1)
    except dns.resolver.NoNameservers:
        logger.error("DNS Server refused to resolve recursively - \
            are you asking for the correct record from the correct hostedZone?")
        sys.exit(1)
    return answers

def check_resolution(dnshost, record_type, dns_resolver):
    """Test name given can resolve using google or whatever nameservers the host running this script defaults to"""
    if dns_resolver == 'google':
        dns.resolver.default_resolver = dns.resolver.Resolver(configure=False)
        dns.resolver.default_resolver.nameservers = [ '8.8.8.8', '8.8.1.1' ]
    else:
        nameservers = 'default'
    try:
        answers = dns.resolver.resolve(dnshost, record_type)
        nameservers = dns.resolver.default_resolver.nameservers[0]
        logging.info('Testing DNS %s record for %s against HostedZone ID %s using resolver %s', \
            record_type, dnshost, hostedzone_id, str(nameservers))
        if record_type == 'A':
            for resource_record in answers:
                logger.info(resource_record.address)
        logging.info('DNS record for %s against HostedZone ID %s successfully resolved', \
            dnshost, hostedzone_id)
    except dns.resolver.NoAnswer:
        logger.error("Error: No answer received from Route53 nameservers -\
            may be an intermittent DNS failure or maybe the HostedZone information has changed")
        sys.exit(1)
    except dns.resolver.NXDOMAIN:
        logger.error("Error: NXDOMAIN received - this is very, very wrong, investigate urgently")
        sys.exit(1)
    except dns.resolver.Timeout:
        logger.error("Error: Timeout from querying Route53 DNS endpoint")
        sys.exit(1)
    except dns.resolver.NoNameservers:
        logger.error("DNS Server refused to resolve recursively - \
            are you asking for the correct record from the correct hostedZone?")
        sys.exit(1)
    return answers


if __name__ == '__main__':
    try:
        host = sys.argv[1]
        hostedzone_id = sys.argv[2]
        record_type = sys.argv[3]
    except IndexError:
        raise SystemExit(f"Usage: {sys.argv[0]} <dns name> <Route53 HostedZoneID> <recordset type> <local|route53|googledns>")
    try:
        dns_resolver = sys.argv[4]
    except IndexError:
        dns_resolver = 'route53'
        pass
    if dns_resolver == 'route53':
        check_resolution_route53(host, hostedzone_id, record_type)
    else:
        check_resolution(host, record_type, dns_resolver)
        
    logger.info('All tests resolved successfully')
    sys.exit(0)
