
# README #



### What is this repository for? ###
This Packer HCL creates a Amazon AMI to use as a Bitbucket Runner

 
### Background ###

This customises a Debian base image as per https://support.atlassian.com/bitbucket-cloud/docs/set-up-and-use-runners-for-linux/, installing docker, tweaks some settings and sets up the Bitbucket runner container to run as a service.

Currently the AMI builds are not pipelined as this is likely to be short lived as Bitbucket will be releasing 4x steps shortly.

### Running & maintenance ###

To think about: If the runner config changes, you'll need to update the HCL file

- Check the base image is still being selected properly on the source_ami_filter - AWS might rename things

- If the runner details have changed, update the `setup-bitbucket-container.sh` file with new run command, remembering to strip out the OAuth secret so it doesn't go into source control and change "-it" to "-d" from the in the docker command so it can run as a service

- Also check the runner name in `docker-bitbucket-pipeline-running.service` and make sure it's consistent with `setup-bitbucket-container.sh`

To build the AMI, first remove the old AMI from the console. Then run:

`aws-vault exec sandbox-access -- packer build -var oauth-client-secret=<oauthsecret> .\aws-bitbucket-runner.pkr.hcl `

Finally remember to clean up any snapshot EBS volumes left behind.