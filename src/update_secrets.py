#!/usr/bin/env python3
import sys
import os
import boto3
import botocore

def main():
    secret_list = sys.argv[1].split(",")
    session = boto3.session.Session()
    client = boto3.client("secretsmanager")
    for secret in secret_list:
        secret_value = os.getenv(secret)
        if secret_value is None:
            print("BitBucket variable for {} may not be set.".format(secret))
        secret_name = secret.replace("___", "/", 1)
        print("Secret name = {}".format(secret_name))
        client.update_secret(
            SecretId=secret_name,
            SecretString=secret_value
        )
        print("Updated value of {}".format(secret))

if __name__ == "__main__":
    main()
