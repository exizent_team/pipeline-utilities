#!/usr/bin/env python3
"""Script to esnure our Bitbucket Pipelines are being deployed to frequently
to ensure we are patched and pushing through to prod frequently"""
import os
import re
from datetime import datetime, timezone
from datetime import timedelta
import logging
import requests
import apprise

# Fetch Bitbucket API access token from environment variable
BITBUCKET_ACCESS_TOKEN = os.environ.get('BITBUCKET_ACCESS_TOKEN')
BITBUCKET_WORKSPACE = os.environ.get('BITBUCKET_WORKSPACE')
BITBUCKET_REPO_SLUG = os.environ.get('BITBUCKET_REPO_SLUG')


# Bitbucket API endpoint for deployments
API_URL = f'https://api.bitbucket.org/2.0/repositories/{
    BITBUCKET_WORKSPACE}/{BITBUCKET_REPO_SLUG}/'


def get_last_successful_deployment(environment):
    """
    Retrieve the last successful deployment from the list of deployments for an environment
    """
    deployments_url = f'{API_URL}/deployments?environment={
        environment}&state.status=SUCCESSFUL&sort=-state.completed_on'
    headers = {'Authorization': f'Bearer {BITBUCKET_ACCESS_TOKEN}'}
    response = requests.get(deployments_url, headers=headers, timeout=10)
    response.raise_for_status()
    last_successful_deployment = response.json()['values'][0]
    return last_successful_deployment


def get_environments():
    """
    Retrieve the list of deployments for the specified repository using the access token.
    """
    environments_url = f'{API_URL}/environments'
    headers = {'Authorization': f'Bearer {BITBUCKET_ACCESS_TOKEN}'}
    response = requests.get(environments_url, headers=headers, timeout=10)
    response.raise_for_status()
    environment_response = response.json()['values']
    # Create a dictionary to store the uuids and names for each environment
    # to use as a look up table later
    environment_id_names = {}
    for environment in environment_response:
        environment_id_names[environment['name']] = environment['uuid']
    return environment_id_names


def check_how_far_behind_environments(deployed_bitbucket_build_numbers, tolerance):
    """
    Work out the gap between oldest and newwest build numbers
    """
    smallest = min(deployed_bitbucket_build_numbers)
    largest = max(deployed_bitbucket_build_numbers)
    # Calculate the difference
    difference = largest - smallest
    # Check if the difference is greater than 10
    return difference > tolerance


def main():
    """ Main """
    environments = get_environments()
    deployed_bitbucket_build_numbers = []
    for environment, environment_uuid in environments.items():
        last_deployment_details = get_last_successful_deployment(
            environment_uuid)
        bitbucket_build_number = int(
            re.sub(r'[^0-9]', '', last_deployment_details['release']["name"]))
        deployed_bitbucket_build_numbers.append(bitbucket_build_number)
        build_timestamp = (datetime.fromisoformat(
            last_deployment_details['state']['completed_on']).astimezone(timezone.utc))
        time_since_build = datetime.now().astimezone(timezone.utc) - build_timestamp

        apobj = apprise.Apprise()

        workflow = os.environ["BITBUCKET_VERSION_CHECKER_NOTIFICATION_WORKFLOW"]

        apobj.add(workflow)


        if time_since_build > timedelta(days=7):
            logging.error("The build deployed to %s for %s was built %d days ago",
                          environment, BITBUCKET_REPO_SLUG, time_since_build.days)
            logging.error(
                "This is greater than 7 days old and potentially vulnerable.")
            logging.error("Please run the deployment pipeline")
            apobj.notify(
                title=f"Deployments in {BITBUCKET_REPO_SLUG} are older than 7 days old",
                body=f"The build running in the \"{environment}\" environment in repo {BITBUCKET_REPO_SLUG} is older than 7 days old - ensure pipeline is run - https://bitbucket.org/exizent_team/{BITBUCKET_REPO_SLUG}/deployments",
            )
            logging.info(
                "Sent notification to MS Teams")
        else:
            logging.info("Pipelines are up to date, no action required")


if __name__ == '__main__':
    main()
