'''Test Harness for Bitbucket deployment check script'''
from unittest.mock import patch
from datetime import datetime, timezone
from datetime import timedelta
import pytest

# Mock data

MOCK_ENVIRONMENTS = {
    "env1": "uuid1",
    "env2": "uuid2",
    "env3": "uuid3",
}

MOCK_ENVIRONMENT_RESPONSE = {
    "values": [
        {"name": "env1", "uuid": "uuid1"},
        {"name": "env2", "uuid": "uuid2"},
        {"name": "env3", "uuid": "uuid3"},
    ]
}

MOCK_DEPLOYMENT_RESPONSE = {
    "values": [
        {
            "release": {"name": "build-123"},
            "state": {
                "completed_on": (
                    datetime.now(tz=timezone.utc) - timedelta(days=1)
                ).isoformat()
            },
        }
    ]
}

@pytest.fixture
def mock_env_vars(monkeypatch):
    ''' Mock environment variables '''
    monkeypatch.setenv("BITBUCKET_ACCESS_TOKEN", "mock_token")
    monkeypatch.setenv("BITBUCKET_WORKSPACE", "mock_workspace")
    monkeypatch.setenv("BITBUCKET_REPO_SLUG", "mock_repo")

# Mock requests.get
@patch("bitbucket_deployment_check.requests.get")

def test_get_last_successful_deployment(mock_get, mock_env_vars):
    ''' Mock API response from the Bitbucket Deployments API
        and ensure we're pulling the correct section '''
    mock_get.return_value.json.return_value = MOCK_DEPLOYMENT_RESPONSE
    mock_get.return_value.raise_for_status.return_value = None
    from bitbucket_deployment_check import get_last_successful_deployment
    result = get_last_successful_deployment("uuid1")
    assert result == MOCK_DEPLOYMENT_RESPONSE["values"][0]

# Mock requests.get
@patch("bitbucket_deployment_check.requests.get")
def test_get_environments(mock_get):
    ''' Mock getting the list of Deployment Environments and thir uuids from
    the bibucket API '''
    mock_get.return_value.json.return_value = MOCK_ENVIRONMENT_RESPONSE
    mock_get.return_value.raise_for_status.return_value = None

    from bitbucket_deployment_check import get_environments, API_URL

    result = get_environments()
    mock_get.assert_called_with(f"{API_URL}/environments", headers={"Authorization": "Bearer mock_token"}, timeout=10)
    assert result == MOCK_ENVIRONMENTS


def test_check_how_far_behind_environments():
    ''' Test the function that checks how far behind latest all environments are '''
    # Test case 1: No environments are more than 10 releases behind the latest
    from bitbucket_deployment_check import check_how_far_behind_environments
    assert check_how_far_behind_environments([100, 105, 110], 10) is False

    # Test case 2: One environment is behind more than 6 released behind
    from bitbucket_deployment_check import check_how_far_behind_environments
    assert check_how_far_behind_environments([100, 105, 95], 6) is True

