#!/bin/bash

# Description:
# This script runs terraform apply for a given $planfile
# either in CREATE or DESTROY mode,
# depending on the given $terraformmode.

# Usage
# To use this script call it passing it:
# - the accountnumber: (AWS account number where resources 
#                       will be deployed / are deployed)
# - region: (AWS region where resources will be deployed / are deployed)
# - terraformmode: can be CREATE (to deploy resources)
#                      or DESTROY (to destroy resources)

accountnumber=$1
region=$2
terraformmode=$3
planfile="artifacts/plan-$TFWORKSPACE-$BITBUCKET_BUILD_NUMBER"

echo "planfile: "$planfile""
if [ "$terraformmode" = "CREATE" ]; then
    echo "Terraform apply is running in CREATE mode"
    terraform apply -input=false -no-color "$planfile"
elif [ "$terraformmode" = "DESTROY" ]; then
    echo "Terraform apply is running in DESTROY mode"
    terraform apply -destroy -auto-approve -input=false -no-color "$planfile"

fi
