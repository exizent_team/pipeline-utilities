import unittest
import mock
import requests
from check_build_number import check_build_number, check_stable_build

class BuildTest(unittest.TestCase):

    def _mock_build_number(self, text):
        mock_build_number = mock.Mock()
        mock_build_number.text = text
        return mock_build_number

    @mock.patch("requests.get")
    def test_build_number_matches(self, mock_get):
        mock_resp = self._mock_build_number(text="3000")
        mock_get.return_value = mock_resp
        with self.assertRaises(SystemExit) as ec:
            check_build_number("https://www.exizent.com", "3000")
        self.assertEqual(ec.exception.code, 0)

    @mock.patch("requests.get")
    def test_build_number_does_not_match(self, mock_get):
        mock_resp = self._mock_build_number(text="3000")
        mock_get.return_value = mock_resp
        with self.assertRaises(SystemExit) as ec:
            check_build_number("https://www.exizent.com", "2340")
        self.assertEqual(ec.exception.code, 2)

    def test_check_build_stable(self):
        with self.assertRaises(Exception) as ex:
            check_stable_build()
        self.assertFalse("No arguments being passed" in str(ex.exception))

    def test_bad_args(self):
        with self.assertRaises(Exception) as ex:
            check_stable_build("test", "test", "test", "test")
        self.assertFalse("Bad arguments being passed" in str(ex.exception))

    def test_invalid_args(self):
        with self.assertRaises(Exception) as ex:
            check_stable_build(123, 123, 123, "test")
        self.assertFalse("Invalid arguments being passed" in str(ex.exception))


    def test_ecs_fails(self):
        service = "mock_service"
        cluster = "mock_cluster"
        task_definition = "mock_task_definition"
        ecs = "mock_ecs"
        with self.assertRaises(Exception) as ex:
            check_stable_build(service, cluster, task_definition, ecs)
        self.assertFalse("this should error" in str(ex.exception))

if __name__ == '__main__':
    unittest.main()
    
