#!/bin/bash

set -e

if [[ $# != 3 ]]
    then
        echo "Error: ECR name, tag name and required image tag required, e.g. \"com.exizent.legal/legal-api sandbox production\""
        echo "\Ie, this example will use the production image and tag it as sandbox"
        exit 2
fi

image=$(aws ecr describe-images --repository-name ${1} --image-ids imageTag=${3})

if [[ -z "$image" ]]
    then
        echo $image
        image_manifest=$(aws ecr batch-get-image --repository-name ${1} --image-ids imageTag=${3} --query 'images[].imageManifest' --output text)
        aws ecr put-image --repository-name ${1} --image-tag ${2} --image-manifest "$image_manifest"
        rvp_manifest=$(aws ecr batch-get-image --repository-name ${1}-rpv --image-ids imageTag=${3} --query 'images[].imageManifest' --output text)
        aws ecr put-image --repository-name ${1}-rvp --image-tag ${2} --image-manifest "$rvp_manifest"
        echo "\Image from ${3} tagged with ${2}"
        exit 0
    else  
        echo "\Image already tagged with ${2}"
        exit 2
fi
exit 2