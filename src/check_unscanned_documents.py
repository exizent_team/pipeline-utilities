#!/usr/bin/env python3
import sys
import json
import boto3

def main():
    """Script that lists fss objects, checks their tags
    to identify if they haven't been scanned by the av-lambda.
    When unscanned objects are found, they are passed to the av-lambda.

    The av-lambda is normally triggered on object creation
    and is passed an ObjectCreated:Put event. This script
    mimics the ObjectCreated:Put event structure to pass
    any unscanned objects to the av-lambda.

    Arguments:  sys.argv[1] --> fss-bucket name
                sys.argv[2] --> aws environment (used to trigger the right av-lambda)

    Raises: Exception as err --> any exception on calling boto3

    Returns: Prints
                unscanned objects found
                unscanned objects that are passed to the av-lambda
                total number of unscanned objects found

    """
    BUCKET_NAME = sys.argv[1]
    AWS_ENVIRONMENT = sys.argv[2]
    s3_client = boto3.client('s3')
    lambda_client = boto3.client('lambda')

    try:
        response = s3_client.list_objects_v2(Bucket=BUCKET_NAME)
        bucket_objects = response['Contents']

        while "NextContinuationToken" in response:
            response = s3_client.list_objects_v2(Bucket=BUCKET_NAME,
                                                 MaxKeys=1000,
                                                 ContinuationToken=response['NextContinuationToken'])

            bucket_objects.extend(response['Contents'])

    except Exception as err:
        print(err)

    unscanned_document_count = 0
    for item in bucket_objects:
        try:
            object_tags = s3_client.get_object_tagging(Bucket=BUCKET_NAME, Key=item["Key"])

            if "av-status" not in str(object_tags["TagSet"]):
                # For debugging purposes only: Commented out to avoid revealing production PII in pipeline log
                # print(f'Found unscanned document: {item["Key"]}')

                object_to_scan = s3_client.get_object(Bucket=BUCKET_NAME, Key=item["Key"])

                event = {
                    "Records":[
                        {
                            "eventName":"ObjectCreated:Put",
                            "s3":{
                                "bucket":{
                                    "name":BUCKET_NAME,
                                    "arn":f"arn:aws:s3:::{BUCKET_NAME}"
                                },
                                "object":{
                                    "key":item["Key"],
                                    "versionId":object_to_scan["VersionId"],
                                }
                            }
                        }
                    ]
                }
                # For debugging purposes only: Commented out to avoid revealing production PII in pipeline log
                # print(f'Scanning: {item["Key"]}')

                lambda_client.invoke(FunctionName=f'exizent-fss-virus-scan-{AWS_ENVIRONMENT}',
                                     InvocationType='Event',
                                     Payload=json.dumps(event))

                unscanned_document_count += 1

            # For debugging purposes only: Commented out to avoid revealing production PII in pipeline log
            # else:
            #     print(f"Object already scanned: {item['Key']}")

        except Exception as err:
            print(err)

    print(f'Number of objects scanned: {unscanned_document_count}')

if __name__ == "__main__":
    main()
