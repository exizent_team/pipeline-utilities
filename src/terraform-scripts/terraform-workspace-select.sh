#!/bin/bash

# Description:
# This script creates or selects (if it already 
# exists) and initialises a terraform workspace,
# named after the deployment environment passed to it.

# Usage
# To use this script call it passing it:
# - the environment: the deployment environment
#                    ie: sandbox, develop
#                        testing, production
# - the accountnumber: (AWS account number where resources 
#                       will be deployed / are deployed)
# - region: (AWS region where resources will be deployed / are deployed)
# - stackname: the name of the stack - used in naming the state file

# The terraform backend is partially configured in this script.
# Do not forget to configure the workspace key prefix (the path
# and name of the state file) in mian.tf
# eg:
# backend "s3" {
#     workspace_key_prefix = "terraform/mongodb-atlas"
# }

environment=$1
accountnumber=$2
region=$3
stackname=$4

echo "Backend-config bucket: bucket=exizent-infx-versioned-artifacts-"$accountnumber"-"$region""

terraform init \
    -backend-config="bucket=exizent-infx-versioned-artifacts-"$accountnumber"-"$region"" \
    -backend-config="key="$stackname"-"$accountnumber"-"$region"" \
    -backend-config="region="$region"" \
    -input=false \
    -no-color

if terraform workspace list | grep -q "$environment"
    then
        echo "workspace "$environment" found."
        terraform workspace select "$environment"
        echo "switched to workspace "$environment""
    else
        echo "workspace "$environment" not found."
        echo "terraform workspace new "$environment""
        terraform workspace new "$environment"
fi
