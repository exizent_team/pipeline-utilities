import unittest
import os
from msteams_bb_deployment_status import get_card_color, StageEnum, send_msg

class TestTeams(unittest.TestCase):

    def test_starting_colour(self):
        result = get_card_color(StageEnum.STARTING)
        self.assertEqual(result, "#FFC599")

    def test_undefined_colour(self):
        result = get_card_color("any_colour")
        self.assertEqual(result, "#FFFFFF")

    def test_invalid_url(self):
        result = send_msg("https://exizent.com","UNIT TEST","123","https://exizent.com",StageEnum.STARTING,"script helper")
        self.assertEqual(result,500)

    def test_post(self):
        result = send_msg(os.environ.get('TEAMS_TEST_WEBHOOK'),"UNIT TEST","123","https://exizent.com",StageEnum.STARTING,"script helper")
        self.assertEqual(result,200)

if __name__ == '__main__':
    unittest.main()
    