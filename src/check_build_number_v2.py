#!/usr/bin/env python3
'''For a given ECS Service, Cluster and Build number URL, call ECS, trigger a forced
update of the given service, ensure the service gets to a a stable state, and when it
has, check it is running the build from this pipeline run. If the builds don't match,
it would indicate that ECS couldnt start the service with the containers from this build
and the pipeline should fail'''

import time
import logging
import sys
import requests
import boto3
import botocore

def check_build_number(url, build):
    '''Perform GET against endpoint to extract build number being served'''
    request = requests.get(url)
    request_text = request.text
    if request_text.strip() == build:
        print("Build number is correct - exiting with exit code 0")
        sys.exit(0)
    if request_text.strip() == "":
        print("There is no build number. Please check the static file exists for the application.")
        sys.exit(2)
    else:
        print("Build number is not correct")
        print("Waiting 10 seconds")
    print("Exiting with exit code 2")
    sys.exit(2)

def check_stable_build(service, cluster, task_definition, ecs):
    ''' Poll the ECS API every 15 seconds until a successful state is reached.
    An error is returned after 40 failed checks. '''
    waiter = ecs.get_waiter("services_stable")
    print("Waiter {}".format(waiter))
    try:
        print("waiting for {} to become stable".format(service))
        waiter.wait(cluster=cluster, services=[service])
        print("{} is stable".format(service))
    except botocore.exceptions as ex:
        logging.error("The service didn't become stable. %i", format(ex))

def get_task_definition(service, cluster):
    '''Retrieve the Task Definition ARN associated with a service on a given cluster'''
    session = boto3.session.Session()
    ecs = session.client("ecs")
    response = ecs.describe_services(
        cluster = cluster,
        services = [service]
    )
    task_definition_arn = response['services'][0]['taskDefinition']
    return task_definition_arn

def main():
    ''' Compare the build number on the service with the build number from the pipeline '''
    service = sys.argv[1]
    cluster = sys.argv[2]
    task_definition = get_task_definition(service, cluster)
    print(task_definition)
    url = None
    build = None
    print(len(sys.argv))
    if len(sys.argv) > 3:
        url = sys.argv[3]
        build = "BITBUCKET_BUILD_NUMBER={}".format(sys.argv[4])
    session = boto3.session.Session()
    ecs = session.client("ecs")
    ecs.update_service(cluster=cluster, service=service,
            taskDefinition=task_definition )
    check_stable_build(service, cluster, task_definition, ecs)
    if None not in (url, build):
        print("Waiting 15 seconds to test the build number")
        time.sleep(15)
        check_build_number(url, build)

if __name__ == "__main__":
    main()
