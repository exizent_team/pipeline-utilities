#!/bin/bash

# Description:
# This script runs terraform plan and outputs
# it in a $planfile. It can be run in
# CREATE or DESTROY mode, depending on the
# given $terraformmode.

# Usage
# To use this script call it passing it:
# - the accountnumber: (AWS account number where resources will be deployed / are deployed)
# - region: (AWS region where resources will be deployed / are deployed)
# - terraformmode: can be CREATE (to deploy resources)
#                      or DESTROY (to destroy resources)

accountnumber=$1
region=$2
terraformmode=$3
planfile="artifacts/plan-$TFWORKSPACE-$BITBUCKET_BUILD_NUMBER"

echo "planfile: "$planfile""

mkdir -p artifacts

if [ "$terraformmode" = "CREATE" ]; then
    echo "Terraform plan is running in CREATE mode"
    terraform plan -input=false -no-color -out "$planfile"
elif [ "$terraformmode" = "DESTROY" ]; then
    echo "Terraform plan is running in DESTROY mode"
    terraform plan -destroy -input=false -no-color -out "$planfile"
fi
