#!/usr/bin/env python3
'''For a given Amplify App & branch this script will trigger a build / test deploy
job and poll for successful or failed Amplify build / test / deploy job '''

import time
import logging
import sys
import boto3
import botocore


def poll_amplify_deploy_job(app_id, branch_name, job_id):
    ''' A waiter function to poll the Amplify API to look for a successful 
    Amplify build / test / deploy job '''
    session = boto3.session.Session()
    amplify = session.client("amplify")

    job_complete = False
    while (job_complete is False):
        job_status = amplify.get_job(
            appId=app_id,
            branchName=branch_name,
            jobId=job_id
        )
        if job_status['job']['summary']['status'] in ['PENDING', 'PROVISIONING', 'RUNNING', 'CANCELLING']:
            print("Amplify deployment job is still running.")
            time.sleep(15)
        elif job_status['job']['summary']['status'] == 'SUCCEED':
            print("Amplify deployment job has completed successfully")
            return True
        else:
            print("Amplify deployment job has failed.")
            print(job_status['job']['summary']['status'])
            return False


def request_amplify_deploy_job(app_id, branch_name, build_id):
    ''' Trigger a new Amiplify Job and return the Job Number'''
    session = boto3.session.Session()
    amplify = session.client("amplify")

    jobs_list = amplify.list_jobs(
        appId=app_id,
        branchName=branch_name,
    )

    job_queue = 'blocked'
    for job in jobs_list['jobSummaries']:
        if job['status'] == 'RUNNING':
            print("Pipeline failed - an Amplify build or deploy job is already running")
            job_queue = 'blocked'
            return 'blocked'
        else:
            job_queue = 'clear'

    if job_queue == 'clear':
        print("Job queue is showing as clear, will start a deployment")
        try:
            job_run_response = amplify.start_job(
                appId=app_id,
                branchName=branch_name,
                jobType='RELEASE',
                jobReason='Bitbucket Pipeline Deployment job for build ' + build_id
            )
        except botocore.exceptions as ex:
            logging.error(
                "Pipeline failed: Unable to request Amplify job start from AWS API. %s", format(ex))
            raise SystemExit(1)

    if job_run_response['ResponseMetadata']['HTTPStatusCode'] != 200:
        logging.error("Pipeline failed: AWS API returned HTTP Code %s", format(
            job_run_response['ResponseMetadata']['HTTPStatusCode']))
        return 'failed'
    else:
        job_id = job_run_response['jobSummary']['jobId']
        return job_id


def main():
    ''' Compare the build number on the service with the build number from the pipeline '''
    app_id = sys.argv[1]
    branch_name = sys.argv[2]

    new_job = request_amplify_deploy_job(app_id, branch_name, 'DEVELOPMENT')
    if new_job == 'blocked':
        raise SystemExit(1)
    else:
        response = poll_amplify_deploy_job(app_id, branch_name, new_job)
        if response:
            raise SystemExit(0)
        else:
            raise SystemExit(1)


if __name__ == "__main__":
    main()
