import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as autoscaling from 'aws-cdk-lib/aws-autoscaling';
import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as ssm from 'aws-cdk-lib/aws-ssm';

interface BitbucketPipelineRunnerStackProps extends cdk.StackProps {
  awsEnvironment: any;
  env: any;
}

export class BitbucketPipelineRunnerStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: BitbucketPipelineRunnerStackProps) {
    super(scope, id, props);

    const vpcId = ssm.StringParameter.valueFromLookup(this, '/infx/vpcid');
    const vpc = ec2.Vpc.fromLookup(this, 'vpc-baseline', {
      vpcId,
    });

    const exizentBitbucketPipelineRunnerAmi = new ec2.GenericLinuxImage({
      'eu-west-2': 'ami-03a1b9c02202328f7',
    });

    const role = new iam.Role(this, 'InstanceRole', {
      assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore')
      ],
      roleName: "BitbuckerRunnerRole"
    });

    const asg = new autoscaling.AutoScalingGroup(this, 'ASG', {
      vpc,
      allowAllOutbound: true,
      associatePublicIpAddress: false,
      autoScalingGroupName: `exizentBitbucketPipelineRunner-${props.awsEnvironment}`,
      desiredCapacity: 0,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T3, ec2.InstanceSize.MICRO),
      machineImage: exizentBitbucketPipelineRunnerAmi,
      minCapacity: 0,
      maxCapacity: 5,
      role: role
    });

    // Set up CPU-based scaling
    asg.scaleOnCpuUtilization('CpuScaling', {
      targetUtilizationPercent: 50
    });

    // Additional scaling policy to allow scale to zero
    asg.scaleOnMetric('AllowScaleToZero', {
      metric: new cloudwatch.Metric({
        namespace: 'AWS/EC2',
        metricName: 'CPUUtilization',
        period: cdk.Duration.minutes(5),
        statistic: 'Average',
      }),
      scalingSteps: [
        { upper: 20, change: -1 },  // If CPU Utilization is below 20%, scale down by 1 instance
      ],
      adjustmentType: autoscaling.AdjustmentType.CHANGE_IN_CAPACITY,
    });

  }
}
