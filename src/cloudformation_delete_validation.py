#!/usr/bin/env python3
import boto3
import sys
import botocore

def check_stack_is_deleted(stack_name, max_attempts, delay):
    client = boto3.client('cloudformation')
    waiter = client.get_waiter('stack_delete_complete')
    try:
        waiter.wait(
            StackName=stack_name, 
            WaiterConfig={
                'Delay': int(delay),
                'MaxAttempts': int(max_attempts)
            }
        )
        print("waiting for {} to be torn down".format(stack_name))
    except botocore.exceptions.WaiterError as error:
        print("Stack was not deleted: {}".format(error))
        sys.exit(2)
    print(waiter)
    sys.exit(0)

def main():
    stack_name = sys.argv[1]
    max_attempts = sys.argv[2]
    delay = sys.argv[3]
    check_stack_is_deleted(stack_name, max_attempts, delay)

if __name__ == "__main__":
    main()