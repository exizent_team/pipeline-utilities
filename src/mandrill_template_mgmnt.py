#!/usr/bin/env python3

"""
Manage Mandrill email templates used by Bereavement Notification platform
This is intended to be run within a deployment pipeline to keep published 
email templated consistent the with the brand template kept in source control.

The Add action is intended to be run by the API pipeline to get a blank template 
available, the update action is intended to be run by the frontend script as 
brand customisations are applied
"""
import argparse
import json
import os
import sys
import mailchimp_transactional as MailchimpTransactional
from mailchimp_transactional.api_client import ApiClientError


def check_non_empty(value):
    """
    Check if the provided argument value is a non-empty string. 
    """
    if not value:
        raise argparse.ArgumentTypeError("A non-empty string is required")
    return value


def mandrill_connect():
    """ 
    Establishes a connection to the Mandrill service using the MailchimpTransactional client.
    This function attempts to initialize and return a client object for interacting with the
    Mandrill API. It requires the MANDRILL_API_KEY to be set in the environment or within the scope
    of the function before calling.
    """
    if "MANDRILL_API_KEY" in os.environ:
        mandrill_api_key = os.getenv('MANDRILL_API_KEY')
        try:
            client = MailchimpTransactional.Client(mandrill_api_key)
            return client
        except ApiClientError as error:
            print(error)
            return False
    else:
        raise EnvironmentError(
            "Failed because MANDRILL_API_KEY environment variable is not set.")


def get_template_code_from_api(brand, template_type):
    """ 
    Retrieves the published code of a specific email template by combining a brand name and a 
    template name to generate a unique template identifier.
    This function connects to the Mandrill API to fetch the template details. If the template 
    exists and is found, the function returns its published code. 
    If the template cannot be found, it returns a specific error message. For any other API 
    errors that occur during the process, the function prints the error details and exits with 
    an error code, failing the deployment pipeline
    """
    client = mandrill_connect()
    brand_template_name = brand + '-' + template_type
    try:
        response = client.templates.info({"name": brand_template_name})
        if response['name'] == brand_template_name:
            return response['publish_code']
    except ApiClientError as error:
        if error.status_code == 404:
            return "TemplateDoesNotExist"
        print(error.status_code)
        print(error.text)
        sys.exit(1)
    print("ApiErrorOccurred")
    return "ApiErrorOccurred"

def get_template_code(brand, template_type):
    """ 
    Load HTML template from wherever it is currently being stored, currently 
    a file within source control, but this is a point to plug in a CMS or 
    a config management call
    """
    source_filename = 'mailchimp_templates/' + brand + '/' + template_type
    try:
        with open(source_filename, encoding="utf-8") as file:
            template_source_code = file.read()
            return template_source_code
    except FileNotFoundError:
        print(f"{source_filename} Template cannot be found in source repo")
        sys.exit(1)

def add_template(brand, template_type, deployment_environment):
    """
    Creates a new, blank, email template in Mandrill, for the update/publish function to use.
    """
    full_template_name = brand + '-' + template_type + '-' + deployment_environment
    client = mandrill_connect()
    try:
        template_add_response = client.templates.add({"name": full_template_name})
        print (f"Tempate {template_add_response['name']} successfully added at {template_add_response['created_at']}")
    except ApiClientError as error:
        if error.text['code'] == 6:
            print ("OK - Template already exists - skipping")
        else:
            print (error.text)
            sys.exit(1)
        
def update_publish_template(brand, template, deployment_environment, template_code):
    """
    Calls Mandrill API to update a given template with a given code block,
    and then calls publish endpoint to ensure the template has been published.
    """
    template_name = brand + '-' + template + '-' + deployment_environment
    client = mandrill_connect()
    try:
        template_update_response = client.templates.update(
            {"name": template_name, "code": template_code}
        )
        print(
            f"{template_update_response['name']} updated at \
              {template_update_response['updated_at']}")
        template_publish_response = client.templates.publish(
            {"name": template_name}
        )
        print(
            f"{template_publish_response['publish_name']} \
                published at {template_publish_response['published_at']}")
    except ApiClientError as error:
        print(error.status_code)
        print(error.text)
        sys.exit(1)

def main():
    """Ensure given mandrill template is in place for given brand and template type"""
    parser = argparse.ArgumentParser(
        description="Create or update Mandrill Email templates")
    parser.add_argument("-a", "--action", type=check_non_empty, required=True,
                        help="Action to be carried out, add an empty email template or \
                              update existing with code from source. Allowed values are \
                              \"add\" or \"update\"")
    parser.add_argument("-b", "--brand", type=check_non_empty, required=True,
                        help="Brand or institution name that we are configuring mail \
                              templates for, e.g. \"exibank\"")
    parser.add_argument("-e", "--environment", type=check_non_empty, required=True,
                        help="Deployment environment we are deploying the template to, \
                              e.g. \"develop\"")
    parser.add_argument("-t", "--templatetype", type=check_non_empty, required=True,
        help="Template type to be uploaded, e.g. ")
    args = parser.parse_args()

    if "MANDRILL_API_KEY" not in os.environ:
        print("Environment / Bitbucket Repository variable \"MANDRILL_API_KEY\" is not set")
        sys.exit(1)

    if args.action == 'add':
        add_template(args.brand, args.templatetype, args.environment,)
    elif args.action == 'update':
        template_code = get_template_code(args.brand, args.templatetype)
        update_publish_template(args.brand, args.templatetype, args.environment, template_code)
    else:
        print(f"Invalid action given - {args.action} - please give either add or update template")
        sys.exit(1)

if __name__ == "__main__":
    main()
