packer {
  required_plugins {
    amazon = {
      version = ">= 1.3.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "oauth-client-secret" {
  type        = string
  description = "The OAuth client secret for registering with bitbucket"
}

source "amazon-ebs" "debian" {
  ami_name                    = "bitbucket-pipeline-runner-workspace"
  instance_type               = "t2.micro"
  region                      = "eu-west-2"
  associate_public_ip_address = "true"
  subnet_id                   = "subnet-005dfed3192d2cbd8"
  vpc_id                      = "vpc-08d4abf9d8dbc6352"
  source_ami_filter {
    filters = {
      name                = "*debian-12-amd64-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["136693071363"]
  }
  ssh_username = "admin"
}

build {
  name = "bitbucket-pipeline-runner"
  sources = [
    "source.amazon-ebs.debian"
  ]

  provisioner "shell" {
    script = "install-docker.sh"
  }

  provisioner "shell" {
    inline = [
      "echo \"Secret = ${var.oauth-client-secret}\"",
    ]
  }

  provisioner "shell" {
    environment_vars = [
      "OAUTHCLIENTSECRET=${var.oauth-client-secret}",
    ]
    script = "setup-bitbucket-container.sh"
  }

  provisioner "file" {
    source      = "./docker-bitbucket-pipeline-running.service"
    destination = "/tmp/docker-bitbucket-pipeline-running.service"
  }

  provisioner "file" {
    source      = "./sysctl.conf"
    destination = "/tmp/sysctl.conf"
  }

  provisioner "file" {
    source      = "./crontab"
    destination = "/tmp/crontab"
  }

  provisioner "shell" {
    inline = [
      "sudo mv /tmp/docker-bitbucket-pipeline-running.service /etc/systemd/system/docker-bitbucket-pipeline-running.service",
      "sudo systemctl enable docker-bitbucket-pipeline-running.service",
      "sudo mv /tmp/sysctl.conf /etc/sysctl.conf",
      "sudo crontab /tmp/crontab"
    ]
  }
}
